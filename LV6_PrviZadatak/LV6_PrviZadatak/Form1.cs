﻿/*Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) 
i barem 5 naprednih (sin, cos, log, sqrt...) operacija.  */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_PrviZadatak
{
    public partial class Form1 : Form
    {

        double a, b, rezultat;
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rezultat = a + b;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rezultat = a - b;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rezultat = a * b;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rezultat = a / b;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rezultat = Math.Sin(a);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rezultat = Math.Cos(a);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rezultat = Math.Log(a);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rezultat = Math.Sqrt(a);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            label3.Text = rezultat.ToString();
            label3.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rezultat = Math.Pow(a, b);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
