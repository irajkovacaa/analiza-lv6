﻿/*Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u svakoj partiji se odabire nasumični pojam iz liste. 
Omogućiti svu funkcionalnost koju biste očekivali od takve igre. 
Nije nužno crtati vješala, dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.*/  
 using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_DrugiZadatak
{
    public partial class Form1 : Form
    {

        int br = 6;
        string path = "C:\\Users\\Ivan\\Desktop\\rijeci.txt";             
        List<string> rijeci = new List<string>();
        int indeks_trazene;
        int brojac_pogodaka;
        string trazena_rijec;
        string crtice;
        System.Text.StringBuilder crticeBuild;

        public Form1()
        {
            InitializeComponent();
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    rijeci.Add(line);
                }
            }
            NewGame();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button24_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button_click(object sender, EventArgs e)
        {

            bool pogodak = false;
            label1.Text = br.ToString();
            Button button = (Button)sender;
            button.Enabled = false;

            for (int i = 0; i < trazena_rijec.Length; i++)
            {

                if (trazena_rijec[i] == Char.ToLower(button.Text[0]))
                {
                    brojac_pogodaka++;
                    crticeBuild[i * 2] = Char.ToLower(button.Text[0]);
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            label3.Text = crtice;
            if (!pogodak)
            {
                br--;
                label1.Text = br.ToString();
            }

            Pobjeda();
            Gubitak();
        }

        public void NewGame()
        {
            crtice = "";
            brojac_pogodaka = 0;
            br = 6;
            Random rand_broj = new Random();
            indeks_trazene = rand_broj.Next(0, rijeci.Count);
            trazena_rijec = rijeci[indeks_trazene];
            for (int i = 0; i < trazena_rijec.Length; i++)
            {
                crtice += "_ ";
            }
            crticeBuild = new StringBuilder(crtice);
            label1.Text = br.ToString();
            label3.Text= crtice;
            foreach (Control c in Controls)
            {
                Button b = c as Button;
                if (b != null)
                {
                    b.Enabled = true;
                }
            }
        }

        
        public void Pobjeda()
        {
            if (brojac_pogodaka == trazena_rijec.Length)
            {
                MessageBox.Show("Pobjedili ste!");
                NewGame();
            }
        }
        public void Gubitak()
        {
            if (br <= 0)
            {
                MessageBox.Show("Izgubili ste!");
                NewGame();
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            NewGame();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
